package pop.lab.business;

import pop.lab.beans.User;

public interface Calculator {

    long sum(User user);

}
