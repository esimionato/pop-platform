package pop.lab.business;

import pop.lab.beans.User;
import pop.lab.beans.Data;
import pop.lab.data.Datator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculatorImpl implements Calculator {

    @Autowired
    private Datator datator;

    //@Autowired
    //public CalculatorImpl(Datator datator) {
    //    super();
    //    this.datator = datator;
    //}

    public long sum(User user) {
        long sum = 0;
        for (Data data: datator.retrieveData(user)) {
            sum += data.getValue();
        }
        return sum;
    }

}
