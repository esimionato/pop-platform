package pop.lab.app;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import com.mysql.jdbc.Driver;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;



public class JdbcDepth {

    public static final String  COUNT_LOGINS = " select count(distinct(gatu_id)) "
            + " from xlogs_historico "
            + " where log_date = ?";

    public static final String  COUNT_POCKET_LOGINS = " select count(distinct(gatu_id)) "
            + " from xlogs_historico "
            + " where log_date = ? "
            + "  and result = 'LOG-IN-API' ";


    public static final String  COUNT_CREATED_USERS = " select count(1) "
            + " from gaturros "
            + " where created >= ? "
            + " and created <= ? ";

    public static final String  COUNT_UNIQUE_CREATED_USERS = " select count(1) "
            + " from gaturros g, tutors t "
            + " where g.tutor_id = t.tutor_id "
            + " and g.created >= ? "
            + " and g.created <= ? "
            + " and t.tutor_created >= ? "
            + " and t.tutor_created <= ? " ;

    public static final String  COUNT_D30_LOGINS = " select count(distinct(gatu_id)) "
            + " from xlogs_historico "
            + " where log_date >= ? "
            + "  and log_date <= ? ";

    public static final String  COUNT_D30_POCKET_LOGINS = " select count(distinct(gatu_id)) "
            + " from xlogs_historico "
            + " where log_date >= ? "
            + "  and log_date <= ? "
            + "  and result = 'LOG-IN-API' ";



    public static final String  COUNT_D30_CREATED_USERS = " select count(1) "
            + " from gaturros "
            + " where created >= ? "
            + " and created <= ? ";

    public static final String  COUNT_D30_UNIQUE_CREATED_USERS = " select count(1) "
            + " from gaturros g, tutors t "
            + " where g.tutor_id = t.tutor_id "
            + " and g.created >= ? "
            + " and g.created <= ? "
            + " and t.tutor_created >= ? "
            + " and t.tutor_created <= ? " ;



    private static  String configPath = "config.properties";
    private static  String dbHost = "";
    private static  String dbPort = "";
    private static  String dbName = "";
    private static  String dbUsername = "";
    private static  String dbPassword = "";

    private static String googleSpread = "";
    private static String googleSheet = "";
    private static String googlePivotCell = "";


    private static SimpleDriverDataSource  ds;
    private static JdbcTemplate jdbc;
    private static DateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
    private static DateFormat yyyyMMddhhmmss = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private static Date day;

    private static int dau = 0;
    private static int pdau = 0;
    private static int created = 0;
    private static int uniqueCreated = 0;
    private static int mau = 0;
    private static int pmau = 0;
    private static int mCreated = 0;
    private static int mUniqueCreated = 0;


    public static void main(String args[]) throws SQLException, ParseException, IOException {
        evaluateArguments(args);
        loadProperties();
        setup();
        calculateMetrics();
        printResults();
        pushToSheet();

        do {
            day = getNextDay(day);
            calculateMetrics();
            printResults();
            pushToSheet();
        } while (!isToday(day));
    }

    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    public static boolean isSameDay(Date d1, Date d2) {
        if (d1 == null | d2 == null) {
            throw new IllegalArgumentException("the dates must not be null");
        }
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(d2);
        return isSameDay(c1, c2);
    }

    public static boolean isSameDay(Calendar c1, Calendar c2) {
        if (c1 == null || c2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (c1.get(Calendar.ERA) == c2.get(Calendar.ERA) &&
                c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
                c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR));
    }

    public static Date getNextDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }


    public static void setup() throws SQLException, ParseException, IOException {
        Quicker.init();

        ds = new SimpleDriverDataSource();
        ds.setDriver(new com.mysql.cj.jdbc.Driver());
        ds.setUrl("jdbc:mysql://"+dbHost+":"+dbPort+"/"+dbName);
        ds.setUsername(dbUsername);
        ds.setPassword(dbPassword);
        jdbc = new JdbcTemplate(ds);
    }

    public static void pushToSheet() throws IOException {
        Quicker.appendRowValues(
                (googleSheet+"!"+googlePivotCell),
                googleSpread,
                yyyyMMdd.format(day),
                String.valueOf(mau),
                String.valueOf(mau-pmau),
                String.valueOf(mau),
                String.valueOf(created),
                String.valueOf(uniqueCreated));

    }

    /********************** METRICAS ******************/

    public static void calculateMetrics() {
        dau = countDAU(day);
        pdau = countPDAU(day);
        created = countCreated(day);
        uniqueCreated = countUniqueCreated(day);
        mau = countMAU(day);
        pmau = countPMAU(day);
        mCreated = countMCreated(day);
        mUniqueCreated = countMUniqueCreated(day);
    }

    public static int countDAU(Date d) {
        int count =  jdbc.queryForObject(
                COUNT_LOGINS,
                new Object[] { yyyyMMdd.format(d) },
                Integer.class);
        return count;
    };


    public static int countPDAU(Date d) {
        int count =  jdbc.queryForObject(
                COUNT_POCKET_LOGINS,
                new Object[] { yyyyMMdd.format(d) },
                Integer.class);
        return count;
    };


    public static int countCreated(Date d) {
        int count =  jdbc.queryForObject(
                COUNT_CREATED_USERS,
                new Object[] {
                    getStartOfDay(d),
                    getEndOfDay(d)
                },
                Integer.class);
        return count;
    };

    public static int countUniqueCreated(Date d) {
        int count =  jdbc.queryForObject(
                COUNT_UNIQUE_CREATED_USERS,
                new Object[] {
                    getStartOfDay(d),
                    getEndOfDay(d),
                    getStartOfDay(d),
                    getEndOfDay(d)
                },
                Integer.class);
        return count;
    };


    public static int countMAU(Date d) {
        int count =  jdbc.queryForObject(
                COUNT_D30_LOGINS,
                new Object[] {
                    yyyyMMdd.format(get30DaysBefore(d)),
                    yyyyMMdd.format(d)
                },
                Integer.class);
        return count;
    };

    public static int countPMAU(Date d) {
        int count =  jdbc.queryForObject(
                COUNT_D30_POCKET_LOGINS,
                new Object[] {
                    yyyyMMdd.format(get30DaysBefore(d)),
                    yyyyMMdd.format(d)
                },
                Integer.class);
        return count;
    };

    public static int countMCreated(Date d) {
        int count =  jdbc.queryForObject(
                COUNT_D30_CREATED_USERS,
                new Object[] {
                    getStartOfDay(get30DaysBefore(d)),
                    getEndOfDay(d)
                },
                Integer.class);
        return count;
    };

    public static int countMUniqueCreated(Date d) {
        int count =  jdbc.queryForObject(
                COUNT_UNIQUE_CREATED_USERS,
                new Object[] {
                    getStartOfDay(get30DaysBefore(d)),
                    getEndOfDay(d),
                    getStartOfDay(get30DaysBefore(d)),
                    getEndOfDay(d)
                },
                Integer.class);
        return count;
    };




    /********************* FECHAS *********************/

    public static Date getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 23, 59, 59);
        return calendar.getTime();
    }

    public static Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 00, 00, 00);
        return calendar.getTime();
    }

    public static Date get30DaysBefore(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -30);
        return calendar.getTime();
    }

    public static boolean isDate(String strDate) {
        try {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            sf.setLenient(false);
            sf.parse(strDate);
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }


    /***************************************************************/

    public static void evaluateArguments(String args[]) throws ParseException {
        if(args.length > 0) {
            String argDate = args[0];
            if(!isDate(argDate)) {
                System.out.println("|  La fecha " + argDate + " no es valida, el formato esperado es yyyy-MM-DD.");
                System.exit(1);
            }
            day = yyyyMMdd.parse(argDate);
            System.out.println(" |  Se procesa la fecha del argumento : " + yyyyMMdd.format(day) + "    \t\t|");
        } else {
            day = new Date();
            System.out.println("\t\t |  No se paso una fecha, se toma la fecha de hoy " + yyyyMMdd.format(day) + ".");
        }

        if(args.length > 1) {
            configPath = args[1];
            System.out.println(" |  Se toma la configuracion del archivo : " + configPath + "\t|");
        }
    }


    public static void printResults() {
        System.out.printf("\n\t  Metrics of Day " + yyyyMMddhhmmss.format(getStartOfDay(day)) + " to " + yyyyMMddhhmmss.format(getEndOfDay(day)) + "\n");
        System.out.printf("\n\t  - Total Logins: " +  dau );
        System.out.printf("\n\t  - MMO  Logins:\t" + (dau - pdau));
        System.out.printf("\n\t  - Pocket Logins:\t" + pdau );
        System.out.printf("\n\t  - Created:\t" +  created );
        System.out.printf("\n\t  - Unique Created:\t" +  uniqueCreated);
    }


    public static void loadProperties() throws ParseException, IOException {
        Properties prop = new Properties();
        InputStream input = null;
        input = new FileInputStream(configPath);
        prop.load(input);

        if(prop.getProperty("db.host") != null) {
            dbHost = prop.getProperty("db.host");
        } else {
            System.out.println("| el db.host no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("db.port") != null) {
            dbPort = prop.getProperty("db.port");
        } else {
            System.out.println("| el db.port no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("db.name") != null) {
            dbName = prop.getProperty("db.name");
        } else {
            System.out.println("| el db.name no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("db.username") != null) {
            dbUsername = prop.getProperty("db.username");
        } else {
            System.out.println("| el db.username no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("db.password") != null) {
            dbPassword = prop.getProperty("db.password");
        } else {
            System.out.println("| el db.password no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("google.spreadsheet") != null) {
            googleSpread = prop.getProperty("google.spreadsheet");
        } else {
            System.out.println("| el spreadsheet  no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("google.sheet") != null) {
            googleSheet = prop.getProperty("google.sheet");
        } else {
            System.out.println("| el sheet  no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("google.pivot_cell") != null) {
            googlePivotCell = prop.getProperty("google.pivot_cell");
        } else {
            System.out.println("| el pivot cell  no puede estar vacio, por ejemplo A1.");
            System.exit(1);
        }
    }
}
