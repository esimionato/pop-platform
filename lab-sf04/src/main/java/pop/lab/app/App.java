package pop.lab.app;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Configuration
@ComponentScan(basePackages = { "pop" })
class SpringContext {

}

public class App {

    public static void main(String args[]) {
        ApplicationContext ctx =
            //new ClassPathXmlApplicationContext("context.xml");
            new AnnotationConfigApplicationContext(SpringContext.class);
        System.out.println("\t/-_-/ ... ( hello world )");
    }
}
