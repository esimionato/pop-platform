package pop.lab.app;


import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;

import java.util.Collections;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.io.FileInputStream;


import org.springframework.stereotype.Service;

@Service
public class Quicker {

    private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";

    private static final java.io.File DATA_STORE_DIR = new java.io.File(
            System.getProperty("user.home"), ".credentials/sheets.googleapi.com-javaquickstart");

    private static FileDataStoreFactory DATA_STORE_FACTORY;

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static HttpTransport HTTP_TRANSPORT;

    private static final List<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/spreadsheets");

    public static Sheets sheets;

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    public static Credential authorize() throws IOException {
        Credential credential = GoogleCredential.fromStream(
                new FileInputStream("client_secret.json"))
            .createScoped(SCOPES);
        return credential;
    }


    private static HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
        return new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest httpRequest) throws IOException {
                requestInitializer.initialize(httpRequest);
                httpRequest.setConnectTimeout(1 * 30000);  // 3 minutes connect timeout
                httpRequest.setReadTimeout(1 * 30000);  // 3 minutes read timeout
            }
        };
    }


    public static void init() throws IOException {
        Credential credential = authorize();
        sheets = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, setHttpTimeout(credential))
            .setApplicationName(APPLICATION_NAME)
            .build();
    }

    public static void reset() throws IOException {
        Credential credential = authorize();
        sheets = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, setHttpTimeout(credential))
            .setApplicationName(APPLICATION_NAME)
            .build();
    }

    public static String spreadsheetId = "1xrL0e3sUkLPAkKQVVW0h0HAMMQKqqnj8e-SdktrtpT0";

    public static Object getCellValue(String cell, String spreadsheetId) throws IOException {
        ValueRange valueRange = sheets.spreadsheets().values()
            .get(spreadsheetId, cell)
            .execute();
        return valueRange.getValues().get(0).get(0);
    }

    public static Object updateCellValue(String range, String value, String spreadsheetId) throws IOException {
        List<List<Object>> values = new ArrayList<List<Object>>();
        List<Object> x = new ArrayList<Object>();
        x.add(value);
        values.add(x);
        ValueRange requestBody = new ValueRange();
        requestBody.setValues(values);
        Sheets.Spreadsheets.Values.Update updateRequest =
            sheets.spreadsheets().values().update(spreadsheetId, range, requestBody);

        updateRequest.setValueInputOption("1");

        Object res = null;
        int count = 0;
        int maxTries = 3;
        while(true) {
            try {
                res = updateRequest.execute();
            } catch (Exception e) {
                System.out.println(" | retry count " + count);
                if (++count == maxTries) throw e;
            }
            return res;
        }
    }

    public static Object appendCellValue(String range, String value, String spreadsheetId) throws IOException {
        List<List<Object>> values = new ArrayList<List<Object>>();
        List<Object> x = new ArrayList<Object>();
        x.add(value);
        values.add(x);
        ValueRange requestBody = new ValueRange();
        requestBody.setValues(values);
        Sheets.Spreadsheets.Values.Append appendRequest =
            sheets.spreadsheets().values().append(spreadsheetId, range, requestBody);

        appendRequest.setValueInputOption("1");
        appendRequest.setInsertDataOption("1");

        Object res = null;
        int count = 0;
        int maxTries = 3;
        while(true) {
            try {
                res = appendRequest.execute();
            } catch (Exception e) {
                System.out.println(e);
                System.out.println(" | retry count " + count);
                if (++count == maxTries) throw e;
            }
            return res;
        }
    }

    public static Object appendRowValues(String range, String spreadsheetId, Object... rowValues)
            throws IOException {
        List<List<Object>> rangeValues = new ArrayList<List<Object>>();
        rangeValues.add(Arrays.asList((Object[])rowValues)); //@esimionato
        ValueRange requestBody = new ValueRange();
        requestBody.setValues(rangeValues);
        Sheets.Spreadsheets.Values.Append appendRequest =
            sheets.spreadsheets().values().append(spreadsheetId, range, requestBody);

        appendRequest.setValueInputOption("1");
        appendRequest.setInsertDataOption("1");

        Object res = null;
        int count = 0;
        int maxTries = 5;
        while(true) {
            try {
                res = appendRequest.execute();
            } catch (Exception e) {
                System.out.println(" | retry count " + count);
                if (++count == maxTries) throw e;
            }
            return res;
        }
    }

    public static Object appendListValues(String range, String spreadsheetId, List<List> values)
            throws IOException {
        ValueRange requestBody = new ValueRange();
        List<List<Object>> rangeValues = new ArrayList<List<Object>>();
        values.forEach(i -> {
            List<Object> r1 = new ArrayList<Object>();
            i.forEach(x -> {
                r1.add(x);
            });
            rangeValues.add(r1);
        });
        requestBody.setValues(rangeValues);
        Sheets.Spreadsheets.Values.Append appendRequest =
            sheets.spreadsheets().values().append(spreadsheetId, range, requestBody);

        appendRequest.setValueInputOption("1");
        appendRequest.setInsertDataOption("1");

        Object res = null;
        int count = 0;
        int maxTries = 5;
        while(true) {
            try {
                System.out.println("push... gooogle");
                res = appendRequest.execute();
            } catch (Exception e) {
                System.out.println(" | retry count " + count);
                if (++count == maxTries) throw e;
            }
            return res;
        }
    }

    public static Object clearValues(String range, String spreadsheetId)
        throws IOException {

        Object res = null;
        int count = 0;
        int maxTries = 5;
        while(true) {
        try {
           Sheets.Spreadsheets.Values.Clear request = sheets.spreadsheets().values()
               .clear(spreadsheetId, range, new ClearValuesRequest());
           res = request.execute();

        } catch (Exception e) {
                System.out.println(" | retry count " + count);
                if (++count == maxTries) throw e;
         }
         return res;
        }
    }


}

