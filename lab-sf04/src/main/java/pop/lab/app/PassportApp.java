package pop.lab.app;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.math.BigDecimal;


import lombok.ToString;

import com.mysql.jdbc.Driver;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import org.springframework.jdbc.core.RowMapper;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;


public class PassportApp {

    //public static final String  SP_PASSAPORTES = "{call `reporte_pasaportes_recurrencias_op2392_op3286`(?, ?)}";
    public static final String  SP_PASSAPORTES = "{call `reporte_pasaportes_recurrencias_op3286`(?, ?)}";

    private static  String configPath = "config.properties";
    private static  String dbHost = "";
    private static  String dbPort = "";
    private static  String dbName = "";
    private static  String dbUsername = "";
    private static  String dbPassword = "";

    private static String googleSpread = "";
    private static String googleSheet = "";
    private static String googlePivotCell = "";
    private static String googleIdxSheet = "";
    private static String googleIdxCell = "";

    private static List<List> metrics = new ArrayList<List>();


    private static SimpleDriverDataSource  ds;
    private static JdbcTemplate jdbc;
    private static DateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
    private static DateFormat yyyyMMdd2 = new SimpleDateFormat("dd/MM/yyyy");
    private static DateFormat yyyyMM = new SimpleDateFormat("yyyy/MM");
    private static DateFormat yyyyMMMM = new SimpleDateFormat("yyyy-MMMM");

    private static Date day;

    public static void main(String args[]) throws SQLException, ParseException, IOException {
        evaluateArguments(args);
        loadProperties();
        setup();
        calculateMetrics();
        printResults();
        pushToSheet();
    }

    public static void setup() throws SQLException, ParseException, IOException {
        Quicker.init();

        ds = new SimpleDriverDataSource();
        ds.setDriver(new com.mysql.cj.jdbc.Driver());
        ds.setUrl("jdbc:mysql://"+dbHost+":"+dbPort+"/"+dbName+"?noAccessToProcedureBodies=true");
        ds.setUsername(dbUsername);
        ds.setPassword(dbPassword);
        jdbc = new JdbcTemplate(ds);
    }

    public static void pushToSheet() throws IOException {
        Quicker.clearValues(
                (googleSheet+"!"+googlePivotCell),
                googleSpread);
        Quicker.appendListValues(
                (googleSheet+"!"+googlePivotCell),
                googleSpread,
                metrics);
        Quicker.clearValues(
                (googleIdxSheet+"!"+googleIdxCell),
                googleSpread);
        Quicker.appendCellValue(
                (googleIdxSheet+"!"+googleIdxCell),
                yyyyMMdd2.format(new Date()),
                googleSpread);

    }

    /********************** METRICAS ******************/
    public static void calculateMetrics() {
       String periodo = yyyyMMdd.format(day);
       List result = jdbc.query(SP_PASSAPORTES, new Object[] {periodo, "%Y-%M"}, new MetricRowMapper());
       metrics = ((List<MetricItem>)(Object)result).stream().<List>map(i -> {
           List<Object> l = new ArrayList<Object>();
           l.add(i.gateway);
           l.add(i.descripcion);
           l.add(i.idPortal);
           l.add(i.idMoneda);
           l.add(i.diasProducto);
           l.add(i.anio);
           l.add(i.mes);
           l.add(i.monto);
           l.add(i.cantidad);
           l.add(i.ultDebitoProcesado);
           l.add(i.idProducto);
           l.add(i.idMonedaVenta);
           l.add(i.precioVenta);
           l.add(i.limiteRecurrencias);
          l.add(i.proxProducto);
          l.add(i.proxProductoIdMoneda);
          l.add(i.proxProductoPrecio);
          l.add(i.proxProductoDias);
          l.add(i.periodo);
           return l;
       }).collect(Collectors.toList());
    }

    /********************* FECHAS *********************/

    public static boolean isDate(String strDate) {
        try {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            sf.setLenient(false);
            sf.parse(strDate);
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }


    /***************************************************************/

    public static void evaluateArguments(String args[]) throws ParseException {
        if(args.length > 0) {
            String argDate = args[0];
            if(!isDate(argDate)) {
                System.out.println("|  La fecha " + argDate + " no es valida, el formato esperado es yyyy-MM-DD.");
                System.exit(1);
            }
            //day = yyyyMMdd.parse(argDate);
            day = yyyyMMdd.parse("20180101");
            System.out.println(" |  Se procesa la fecha del argumento : " + yyyyMMdd.format(day) + "    \t\t|");
        } else {
            day = new Date();
            System.out.println("\t\t |  No se paso una fecha, se toma la fecha de hoy " + yyyyMMdd.format(day) + ".");
        }

        if(args.length > 1) {
            configPath = args[1];
            System.out.println(" |  Se toma la configuracion del archivo : " + configPath + "\t|");
        }
    }


    public static void printResults() {
        System.out.println(metrics);
    }


    public static void loadProperties() throws ParseException, IOException {
        Properties prop = new Properties();
        InputStream input = null;
        input = new FileInputStream(configPath);
        prop.load(input);

        if(prop.getProperty("bac.db.host") != null) {
            dbHost = prop.getProperty("bac.db.host");
        } else {
            System.out.println("| el bac.db.host no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("bac.db.port") != null) {
            dbPort = prop.getProperty("bac.db.port");
        } else {
            System.out.println("| el bac.db.port no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("bac.db.name") != null) {
            dbName = prop.getProperty("bac.db.name");
        } else {
            System.out.println("| el bac.db.name no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("bac.db.username") != null) {
            dbUsername = prop.getProperty("bac.db.username");
        } else {
            System.out.println("| el bac.db.username no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("bac.db.password") != null) {
            dbPassword = prop.getProperty("bac.db.password");
        } else {
            System.out.println("| el bac.db.password no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("bac.google.spreadsheet") != null) {
            googleSpread = prop.getProperty("bac.google.spreadsheet");
        } else {
            System.out.println("| el spreadsheet  no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("bac.google.sheet") != null) {
            googleSheet = prop.getProperty("bac.google.sheet");
        } else {
            System.out.println("| el sheet  no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("bac.google.pivot_cell") != null) {
            googlePivotCell = prop.getProperty("bac.google.pivot_cell");
        } else {
            System.out.println("| el pivot cell  no puede estar vacio, por ejemplo A1.");
            System.exit(1);
        }
        if(prop.getProperty("bac.google.idx_sheet") != null) {
            googleIdxSheet = prop.getProperty("bac.google.idx_sheet");
        } else {
            System.out.println("| el idx_sheet  no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("bac.google.idx_cell") != null) {
            googleIdxCell = prop.getProperty("bac.google.idx_cell");
        } else {
            System.out.println("| el idx cell  no puede estar vacio, por ejemplo A1.");
            System.exit(1);
        }
    }

    @ToString(includeFieldNames=false, of={"monto"})
    static class MetricItem {
        public String gateway = "";
        public String descripcion = "";
        public Integer idPortal = 0;
        public Integer idMoneda = 0;
        public Integer diasProducto = 0;
        public Integer anio = 2018;
        public Integer mes = 0;
        public BigDecimal monto = BigDecimal.ZERO;
        public Integer cantidad = 0;
        public String ultDebitoProcesado = "";
        public Integer idProducto = 0;
        public Integer idMonedaVenta = 0;
        public BigDecimal precioVenta = BigDecimal.ZERO;
        public Integer limiteRecurrencias = 0;
        public Integer proxProducto = 0;
        public Integer proxProductoIdMoneda = 0;
        public BigDecimal proxProductoPrecio = BigDecimal.ZERO;
        public Integer proxProductoDias = 0;
        public String periodo = "";
    }

    static class MetricRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            try {
            ResultSetMetaData rsmd = rs.getMetaData();
            MetricItem item = new MetricItem();
            int max = rsmd.getColumnCount();

            item.gateway = rs.getString("Gateway");
            item.descripcion = rs.getString("Descripcion");
            item.idPortal = rs.getInt("id_portal");
            item.idMoneda = rs.getInt("id_moneda");
            item.diasProducto = rs.getInt("Dias_producto");
            item.anio = 2018;
            item.mes = rs.getInt("Mes");
            if(rs.getDate("Ultimo_debito_procesado")!=null) {
                item.ultDebitoProcesado = yyyyMMdd2.format(yyyyMMdd.parse(rs.getDate("Ultimo_debito_procesado").toString()));
            }

            item.monto = new BigDecimal(rs.getString("Monto").replaceAll("\\.", "").replaceAll(",", "."));
            item.cantidad = rs.getInt("Cantidad");
            item.idProducto = rs.getInt("Id_producto");

            item.idMonedaVenta = rs.getInt("Id_moneda_venta");
            if (rs.getString("precio_venta")!=null && rs.getString("precio_venta")!="") {
                item.precioVenta = new BigDecimal(rs.getString("precio_venta").replaceAll("\\.", "").replaceAll(",", "."));
            }
            item.limiteRecurrencias = rs.getInt("Limite_recurrencias");
            item.proxProducto = rs.getInt("Prox_producto");
            item.proxProductoIdMoneda = rs.getInt("Prox_producto_id_moneda");
            if (rs.getString("Prox_producto_precio")!=null && rs.getString("Prox_producto_precio")!="") {
                item.proxProductoPrecio = new BigDecimal(rs.getString("Prox_producto_precio").replaceAll("\\.", "").replaceAll(",", "."));
            }
            item.proxProductoDias = rs.getInt("Prox_producto_dias");

            item.periodo = yyyyMMdd2.format(yyyyMMMM.parse(rs.getString("Periodo")));

            return item;

            } catch (ParseException pe) {
                return false;
            }
        }
    }
}
