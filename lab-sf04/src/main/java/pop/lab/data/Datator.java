package pop.lab.data;

import java.util.List;

import pop.lab.beans.User;
import pop.lab.beans.Data;


public interface Datator {

    List<Data> retrieveData(User user);

}
