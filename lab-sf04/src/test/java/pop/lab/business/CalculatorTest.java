package pop.lab.business;

import pop.lab.beans.User;
import pop.lab.beans.Data;
import pop.lab.business.Calculator;
import pop.lab.data.Datator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorTest {

    private static final User DUMMY_USER = new User("dummy");

    @Mock
    private Datator datator;

    @InjectMocks
    private Calculator calculator = new CalculatorImpl();

    @Test
    public void testSum() {
        BDDMockito.given(datator.retrieveData(Matchers.any(User.class)))
            .willReturn(Arrays.asList(new Data(10), new Data(15), new Data(25)));

        long sum = calculator.sum(DUMMY_USER);
        assertEquals(10+15+25, sum);
    }

}
