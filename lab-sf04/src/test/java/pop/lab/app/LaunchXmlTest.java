package pop.lab.app;

import pop.lab.beans.User;
import pop.lab.business.Calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/context.xml" })
public class LaunchXmlTest {

    private static final  User DUMMY_USER = new User("dummy");

    @Autowired
    private Calculator calculator;

    @Test
    public void test() {
        assertEquals(30, calculator.sum(DUMMY_USER));
    }

}
