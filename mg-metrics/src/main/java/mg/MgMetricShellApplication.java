package mg;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.core.MethodParameter;
import org.springframework.core.convert.converter.Converter;

import org.springframework.shell.CompletionContext;
import org.springframework.shell.CompletionProposal;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.shell.standard.ValueProvider;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import org.springframework.util.StringUtils;

import org.jline.utils.AttributedString;

import java.io.PrintStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.io.IOException;

import static mg.commons.terminal.AnsiColors.*;

import mg.gsheets.*;



@SpringBootApplication
public class MgMetricShellApplication {

    public static void main(String[] args) {
        SpringApplication.run(MgMetricShellApplication.class, args);
    }
}


@Service
class MetricService implements InitializingBean {

    public  final Map<String, GFile> files = new HashMap<>();
    public  final Map<String, GSheet> sheets = new HashMap<>();
    public final Map<String, GCell> cells = new HashMap<>();


    Collection<GFile> findFileByName(String name) {
        return this.files.values()
                .stream()
                .filter(f -> f.name.toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }

    Collection<GSheet> findSheetByName(String label) {
        return this.sheets.values()
                .stream()
                .filter(s -> s.label.toLowerCase().contains(label.toLowerCase()))
                .collect(Collectors.toList());
    }

    Collection<GCell> findCellByRow(String strRow) {
        return this.cells.values()
                .stream()
                .filter(c -> String.valueOf(c.row).equals(strRow))
                .collect(Collectors.toList());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, GFile> fileMap = Stream.of("gaturro_2017.xls", "gaturro_2016.xls", "gaturro_2015.xls")
            .map(name -> new GFile(name))
            .collect(Collectors.toMap(GFile::getName, f -> f));
        this.files.putAll(fileMap);
    }

}


@Service
class ConsoleService {
    public final static String ANSI_YELLOW = "\u001B[33m";
    public final static String ANSI_RESET = "\u001B[0m";

    private final PrintStream out = System.out;

    public void write(String msg, String... args) {
        this.out.print("> ");
        this.out.print(ANSI_YELLOW);
        this.out.printf(msg, (Object[]) args);
        this.out.print(ANSI_RESET);
        this.out.println();
    }

    public void write(String msg, Object... args) {
        this.out.print("> ");
        this.out.print(ANSI_YELLOW);
        this.out.printf(msg, (Object[]) args);
        this.out.print(ANSI_RESET);
        this.out.println();
    }

}

@ShellComponent
class SheetCommands {

    private final ConsoleService console;
    private final Sheeter sheeter;
    private final Quicker quicker;

    public String idxSpreadsheet = "1xrL0e3sUkLPAkKQVVW0h0HAMMQKqqnj8e-SdktrtpT0";
    public String idxSheet = "2017";
    public String idxCell = "A1";
    public Object cellValue = "";

    SheetCommands(ConsoleService console, Sheeter sheeter, Quicker quicker)
        throws IOException {
        this.console = console;
        this.sheeter =  sheeter;
        this.quicker = quicker;
        quicker.init();
    }

    @ShellMethod("change the spreadsheet")
    public void spreadsheet(@ShellOption(defaultValue="") String spreadsheet) {
        if(spreadsheet.isEmpty()) {
            this.console.write("%s", this.idxSpreadsheet);
            return;
        }
        this.idxSpreadsheet = spreadsheet;
        this.console.write(" ... spreadsheet changed.");
    }

    @ShellMethod("change the spreadsheet")
    public void sheet(@ShellOption(defaultValue="") String sheet) {
        if(sheet.isEmpty()) {
            this.console.write("%s", this.idxSheet);
            return;
        }
        this.idxSheet = sheet;
        this.console.write(" ... sheet changed.");
    }


    @ShellMethod("interact with the google file")
    public void follow(@ShellOption() String cell
            , @ShellOption(defaultValue="") String sheet
            , @ShellOption(defaultValue="") String spreadsheet) throws Exception {
        this.idxCell = cell;
        if(!sheet.isEmpty()) {
            this.idxSheet = sheet;
        }
        if(!sheet.isEmpty()) {
            this.idxSpreadsheet = spreadsheet;
        }

        this.idxCell = cell;
        this.cellValue = Quicker.getCellValue(this.idxSheet+"!"+this.idxCell, this.idxSpreadsheet);
        this.console.write("%s", cellValue.toString());
    }

    @ShellMethod("interact with the google file")
    public void refresh() throws Exception {
        this.cellValue = Quicker.getCellValue((this.idxSheet +"!"+ this.idxCell), this.idxSpreadsheet);
        this.console.write(" ... cell readed, value: %s.", cellValue.toString());
    }

    @ShellMethod("interact with the google file")
    public void get(@ShellOption("") String cell, @ShellOption("") String sheet,
            @ShellOption("") String spreadsheet) throws Exception {
        if (spreadsheet.isEmpty()) {
            spreadsheet = this.idxSpreadsheet;
        }
        if (sheet.isEmpty()) {
            sheet = this.idxSheet;
        }
        if (cell.isEmpty()) {
            cell = this.idxCell;
        }
        Object otherValue = Quicker.getCellValue((sheet+"!"+cell), spreadsheet);
        this.console.write(" %s", otherValue);
    }

    @ShellMethod("interact with the google file")
    public void update(@ShellOption(defaultValue="$EMPTY") String value, @ShellOption(defaultValue="") String cell,
            @ShellOption(defaultValue="") String sheet, @ShellOption(defaultValue="") String spreadsheet) throws Exception {
        if (spreadsheet.isEmpty()) {
            spreadsheet = this.idxSpreadsheet;
        }
        if (sheet.isEmpty()) {
            sheet = this.idxSheet;
        }
        if (cell.isEmpty()) {
            cell = this.idxCell;
        }
        if (value.equals("$EMPTY")) {
            value = "";
        }
        Object otherValue = Quicker.updateCellValue((sheet+"!"+cell), value, spreadsheet);
        this.console.write("done !");
    }

    @ShellMethod("interact with the google file")
    public void append(@ShellOption(defaultValue="$EMPTY") String value, @ShellOption(defaultValue="") String cell,
            @ShellOption(defaultValue="") String sheet, @ShellOption(defaultValue="") String spreadsheet) throws Exception {
        if (spreadsheet.isEmpty()) {
            spreadsheet = this.idxSpreadsheet;
        }
        if (sheet.isEmpty()) {
            sheet = this.idxSheet;
        }
        if (cell.isEmpty()) {
            cell = this.idxCell;
        }
        if (value.equals("$EMPTY")) {
            value = "";
        }
        Object otherValue = Quicker.updateCellValue((sheet+"!"+cell), value, spreadsheet);
        this.console.write("done !");
    }


    @ShellMethod("interact with the google file")
    public void appendRange(@ShellOption(defaultValue="") String firstCell,
            @ShellOption(defaultValue="") String sheet, @ShellOption(defaultValue="") String spreadsheet, String... values) throws Exception {
        if (spreadsheet.isEmpty()) {
            spreadsheet = this.idxSpreadsheet;
        }
        if (sheet.isEmpty()) {
            sheet = this.idxSheet;
        }
        if (firstCell.isEmpty()) {
            firstCell = this.idxCell;
        }
        Object otherValue = Quicker.appendRowValues((sheet+"!"+firstCell), spreadsheet, values);
        this.console.write("done !");
    }

}

@Component
class ConnectedPromptProvider implements PromptProvider {
    private final MetricService metricService;

    ConnectedPromptProvider(MetricService metricService) {
        this.metricService = metricService;
    }

    @Override
    public AttributedString getPrompt() {
        String msg = String.format(BK_BLACK+GREEN + BOLD + " %s"
               + RESET + BK_BLACK  + "@%s | "
               + MAGENTA + "[" + YELLOW + "%s:%s" + MAGENTA + "]"
               + MAGENTA + "(" + CYAN + "%s" + MAGENTA + ")"
               + WHITE + " >" + RESET + " ",
                "AGOSTO17", // sheet, page
                "MG_METRICS", // datasheet, file
                "A", // col, row
                "42",
                (false ? "EMPTY": "25000" )); // ms.getCell(col,row).isEmpty(); ms.getCell(col,row).getValue(row);
       msg = msg + RESET;

        return new AttributedString(msg);
    }
}

@Component
class FileValueProvider implements ValueProvider {
    private final MetricService metricService;

    FileValueProvider(MetricService metricService) {
        this.metricService = metricService;
    }

    @Override
    public boolean supports(MethodParameter parameter, CompletionContext completionContext) {
        System.out.println("... file_value_provider::support ...");
        return parameter.getParameterType().isAssignableFrom(GFile.class);
    }

    @Override
    public List<CompletionProposal> complete(MethodParameter parameter,
                                             CompletionContext completionContext,
                                             String[] hints) {
        System.out.println("... file_value_provider::complete ...");
        String currentInput = completionContext.currentWordUpToCursor();
        return this.metricService
            .findFileByName(currentInput)
            .stream()
            .map(f -> String.format("(#%s) %s", f.name, f.url))
            .map(CompletionProposal::new)
            .collect(Collectors.toList());
    }
}



@Component
class SheetValueProvider implements ValueProvider {
    private final MetricService metricService;

    SheetValueProvider(MetricService metricService) {
        this.metricService = metricService;
    }

    @Override
    public boolean supports(MethodParameter parameter, CompletionContext completionContext) {
        return parameter.getParameterType().isAssignableFrom(GSheet.class);
    }

    @Override
    public List<CompletionProposal> complete(MethodParameter parameter,
                                             CompletionContext completionContext,
                                             String[] hints) {
        String currentInput = completionContext.currentWordUpToCursor();
        return this.metricService
            .findSheetByName(currentInput)
            .stream()
            .map(s -> String.format("(#%s) %s", s.label, "@file.xls"))
            .map(CompletionProposal::new)
            .collect(Collectors.toList());
    }
}


@Component
class CellValueProvider implements ValueProvider {
    private final MetricService metricService;

    CellValueProvider(MetricService metricService) {
        this.metricService = metricService;
    }

    @Override
    public boolean supports(MethodParameter parameter, CompletionContext completionContext) {
        return parameter.getParameterType().isAssignableFrom(GCell.class);
    }

    @Override
    public List<CompletionProposal> complete(MethodParameter parameter,
                                             CompletionContext completionContext,
                                             String[] hints) {
        String currentInput = completionContext.currentWordUpToCursor();
        return this.metricService
            .findCellByRow(currentInput)
            .stream()
            .map(c -> String.format("[%s:%s](%s)", c.row, c.col, c.value))
            .map(CompletionProposal::new)
            .collect(Collectors.toList());
    }
}





@Component
class FileConverter implements Converter<String, GFile> {

    private final MetricService metricService;

    private final Pattern pattern = Pattern.compile(".*");

    FileConverter(MetricService metricService) {
        this.metricService = metricService;
    }

    //@Nullable
    @Override
    public GFile convert(String source) {
        return new GFile(source);
        //Matcher matcher = this.pattern.matcher(source);
        //if (matcher.find()) {
        //    String group = matcher.group(1);
        //    if( StringUtils.hasText(group) ) {
        //       // Long id = Long.parseLong(group);
        //       // return this.metricService.findBy(id);
        //       return new GFile(source);
        //    }
        //}
        //return null;
    }
}



@Component
class SheetConverter implements Converter<String, GSheet> {

    private final MetricService metricService;

    private final Pattern pattern = Pattern.compile("\\(#(\\d+)\\).*");

    SheetConverter(MetricService metricService) {
        this.metricService = metricService;
    }

    //@Nullable
    @Override
    public GSheet convert(String source) {
        Matcher matcher = this.pattern.matcher(source);
        if (matcher.find()) {
            String group = matcher.group(1);
            if( StringUtils.hasText(group) ) {
               // Long id = Long.parseLong(group);
               // return this.metricService.findBy(id);
               return new GSheet();
            }
        }
        return null;
    }
}


@Component
class CellConverter implements Converter<String, GCell> {

    private final MetricService metricService;

    private final Pattern pattern = Pattern.compile("\\(#(\\d+)\\).*");

    CellConverter(MetricService metricService) {
        this.metricService = metricService;
    }

    //@Nullable
    @Override
    public GCell convert(String source) {
        Matcher matcher = this.pattern.matcher(source);
        if (matcher.find()) {
            String group = matcher.group(1);
            if( StringUtils.hasText(group) ) {
               // Long id = Long.parseLong(group);
               // return this.metricService.findBy(id);
               return new GCell();
            }
        }
        return null;
    }
}


/*
@Component
class MetricValueProvider implements ValueProvider {
    private final MetricService metricService;

    MetricValueProvider(MetricService metricService) {
        this.metricService = metricService;
    }

    @Override
    public boolean supports(MethodParameter parameter, CompletionContext completionContext) {
        return parameter.getParameterType().isAssignableFrom(Metric.class);
    }

    @Override
    public List<CompletionProposal> complete(MethodParameter parameter,
                                             CompletionContext completionContext,)
                                             String[] hints) {
        String currentInput = completionContext.currentWordUpToCursor();
        return this.metricService
            .findByName(currentInput)
            .stream()
            .map(p -> String.format("(#%s) %s", p.getId(), p.getName()))
            .map(CompletionProposal::new)
            .collect(Collectors.toList());
    }
}
*/


/*
@Component
class MetricConverter implements Converter<String, Metric> {

    private final MetricService metricService;

    private final pattern pattern = Pattern.compile("\\(#(\\d+)\\).*");

    MetricConverter(MetricService metricService) {
        this.metricService = metricService;
    }

    //@Nullable
    @Override
    public Metric convert(String source) {
        Matcher matcher = this.pattern.matcher(source);
        if (matcher.find()) {
            String group = matcher.group(1);
            if( StringUtils.hasText(group) ) {
                Long id = Long.parseLong(group);
                return this.metricService(id);
            }
        }
        return null;
    }
}
*/
