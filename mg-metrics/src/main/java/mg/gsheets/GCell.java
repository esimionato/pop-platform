package mg.gsheets;

public class GCell {
    public Col col;
    public int row;
    public String value;
    public CellStatus status;
    public GSheet sheet;

    @Override
    public String toString() {
        return sheet
           + "[" + row + ":" + col + "]"
           + "("+value+")";
    }
}

