package mg.gsheets;

public enum CellStatus {
    WRITE, DRAFT, READ_ONLY;
}

