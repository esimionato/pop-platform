package mg.gsheets;

public class GFile {
    public String url;
    public String key;
    public String name;
    //public List<Sheet> sheets = new ArrayList<Sheeet>();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "@" + name;
    }

    public GFile(String name) {
        this.name = name;
        this.url = "http://google.gl/gaturro/sheets/"+name;
    }
}

