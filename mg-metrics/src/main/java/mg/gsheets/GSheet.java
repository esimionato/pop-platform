package mg.gsheets;

public class GSheet {
    public GFile gFile;
    public String label;
    public int index;

    @Override
    public String toString() {
        return label + "@" + gFile.name;
    }
}

