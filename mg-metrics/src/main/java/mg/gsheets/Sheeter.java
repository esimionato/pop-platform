package mg.gsheets;

import org.springframework.beans.factory.InitializingBean;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import org.springframework.util.StringUtils;
import java.io.PrintStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.ArrayList;


@Service
public class Sheeter implements InitializingBean {

    public  final Map<String, GFile> files = new HashMap<>();
    public  final Map<String, GSheet> sheets = new HashMap<>();
    public final Map<String, GCell> cells = new HashMap<>();


    Collection<GFile> findFileByName(String name) {
        return this.files.values()
                .stream()
                .filter(f -> f.name.toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }

    Collection<GSheet> findSheetByName(String label) {
        return this.sheets.values()
                .stream()
                .filter(s -> s.label.toLowerCase().contains(label.toLowerCase()))
                .collect(Collectors.toList());
    }

    Collection<GCell> findCellByRow(String strRow) {
        return this.cells.values()
                .stream()
                .filter(c -> String.valueOf(c.row).equals(strRow))
                .collect(Collectors.toList());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, GFile> fileMap = Stream.of("gaturro_2017.xls", "gaturro_2016.xls", "gaturro_2015.xls")
            .map(name -> new GFile(name))
            .collect(Collectors.toMap(GFile::getName, f -> f));
        this.files.putAll(fileMap);
    }

}
