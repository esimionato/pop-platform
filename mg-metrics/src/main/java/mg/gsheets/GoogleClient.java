package mg.gsheets;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.InitializingBean;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;

import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;

import com.google.api.client.util.store.FileDataStoreFactory;

import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;
import com.google.api.services.sheets.v4.Sheets;

import java.util.Collections;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.io.FileInputStream;
import java.util.ArrayList;

//import static mg.gsheets.GoogleSettings.*;


@Service
public class GoogleClient implements InitializingBean {

    /* google applicatio name */
    public static final String APPLICATION_NAME =
        "Google Sheets API Java Quickstart";
        //"Gaturro Metrics Collector";


    /* directory to store google user credentials */
    public static final String DATA_STORE_PATH  =
        System.getProperty("user.home")
        + ".credentials/sheets.googleapi.com-javaquickstart";

    public static String spreadsheetId = "1UhgeRYgP3S4x-I_-oQo8dP5DnXJzgUXdYom9HQRAZkg";


    public static final List<String> SCOPES =
        Arrays.asList(SheetsScopes.SPREADSHEETS_READONLY);


    /* directory to store google user credentials */
    public static final java.io.File DATA_STORE_DIR = new java.io.File(
            System.getProperty("user.home"), ".credentials/sheets.googleapi.com-gaturro-metrics");

    /* global instanse of the {@link FileDataStoreFactory} */
    public static FileDataStoreFactory DATA_STORE_FACTORY;

    /* global instanse of json factory */
    public static final JsonFactory JSON_FACTORY =
        JacksonFactory.getDefaultInstance();

    /* global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

    public Sheets service;

    public static Credential authorize() throws IOException {
        Credential credential = GoogleCredential.fromStream(
                GoogleClient.class.getResourceAsStream("/client_secret.json"))
            .createScoped(Collections.singleton(SheetsScopes.SPREADSHEETS_READONLY));
        return credential;
    }


    public static Sheets getSheetsService() throws IOException {
        Credential credential = authorize();
        System.out.println(".............. ^_^ .................");
        System.out.println(credential);
        return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
            .setApplicationName(APPLICATION_NAME)
            .build();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
//        this.service = getSheetsService();
    }

}
