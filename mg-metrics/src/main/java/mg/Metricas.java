package mg;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import com.mysql.jdbc.Driver;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;



public class Metricas {

    public static final String  COUNT_LOGINS = " select count(distinct(gatu_id)) "
            + " from xlogs_historico "
            + " where log_date = ?";

    public static final String  COUNT_CREATED_USERS = " select count(1) "
            + " from gaturros "
            + " where created >= ? "
            + " and created < ? ";

    public static final String  COUNT_UNIQUE_CREATED_USERS = " select count(1) "
            + " from gaturros g, tutors t "
            + " where g.tutor_id = t.tutor_id "
            + " and g.created > ? "
            + " and g.created < ? "
            + " and t.tutor_created > ? "
            + " and t.tutor_created < ? " ;

    private static  String configPath = "config.properties";
    private static  String dbHost = "";
    private static  String dbPort = "";
    private static  String dbName = "";
    private static  String dbUsername = "";
    private static  String dbPassword = "";


    private static SimpleDriverDataSource  ds;
    private static JdbcTemplate jdbc;

    public static void main(String args[]) throws SQLException, ParseException, IOException {
        Properties prop = new Properties();
        InputStream input = null;

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        String strDate = null;

        if(args.length > 0) {
            String argDate = args[0];
            if(!isDate(argDate)) {
                System.out.println("|  La fecha " + argDate + " no es valida, el formato esperado es yyyy-MM-DD.");
                System.exit(1);
            }
            date = df.parse(argDate);
            strDate = df.format(date);
            System.out.println(" |  Se procesa la fecha del argumento : " + strDate + "    \t\t|");
        } else {
            date = new Date();
            strDate = df.format(date);
            System.out.println("\t\t |  No se paso una fecha, se toma la fecha de hoy " + strDate + ".");
        }

        if(args.length > 1) {
            configPath = args[1];
            System.out.println(" |  Se toma la configuracion del archivo : " + configPath + "\t|");
        }

        input = new FileInputStream(configPath);
        prop.load(input);

        if(prop.getProperty("db.host") != null) {
            dbHost = prop.getProperty("db.host");
        } else {
            System.out.println("| el db.host no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("db.port") != null) {
            dbPort = prop.getProperty("db.port");
        } else {
            System.out.println("| el db.port no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("db.name") != null) {
            dbName = prop.getProperty("db.name");
        } else {
            System.out.println("| el db.name no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("db.username") != null) {
            dbUsername = prop.getProperty("db.username");
        } else {
            System.out.println("| el db.username no puede estar vacio.");
            System.exit(1);
        }
        if(prop.getProperty("db.password") != null) {
            dbPassword = prop.getProperty("db.password");
        } else {
            System.out.println("| el db.password no puede estar vacio.");
            System.exit(1);
        }
        System.out.println("");

        ds = new SimpleDriverDataSource();
        ds.setDriver(new com.mysql.cj.jdbc.Driver());
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        Date nextDate = c.getTime();
        strDate = df.format(date);
        String strNextDate = df.format(nextDate);

        ds.setUrl("jdbc:mysql://"+dbHost+":"+dbPort+"/"+dbName);
        ds.setUsername(dbUsername);
        ds.setPassword(dbPassword);

        jdbc = new JdbcTemplate(ds);

        System.out.println("                //   metrics of " + strDate                + " to " + strNextDate + "\t   //");
        System.out.println("   (-___-) ... //   + logins: " +  countDAU(strDate)      + "\t\t\t\t  //");
        System.out.println("              //   + created: " +  countCreated(strDate, strNextDate)      + "\t\t\t\t //");
        System.out.println("             //   + first_created: " +  countUniqueCreated(strDate, strNextDate) + "\t\t\t\t//");
    }

    public static boolean isDate(String strDate) {
        try {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            sf.setLenient(false);
            sf.parse(strDate);
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    public static int countDAU(String strDate) {
        int count =  jdbc.queryForObject(
                COUNT_LOGINS,
                new Object[] { strDate },
                Integer.class);
        return count;
    };

    public static int countCreated(String strDate, String strNextDate) {
        int count =  jdbc.queryForObject(
                COUNT_CREATED_USERS,
                new Object[] { strDate, strNextDate },
                Integer.class);
        return count;
    };


    public static int countUniqueCreated(String strDate, String strNextDate) {
        int count =  jdbc.queryForObject(
                COUNT_UNIQUE_CREATED_USERS,
                new Object[] { strDate, strNextDate, strDate, strNextDate },
                Integer.class);
        return count;
    };

}
