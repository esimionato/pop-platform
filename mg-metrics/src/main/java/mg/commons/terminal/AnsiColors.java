package mg.commons.terminal;

public class AnsiColors {

    public final static String RESET      = "\u001B[0m";
    public final static String BOLD       = "\u001B[1m";
    public final static String UNDERLINE  = "\u001B[4m";
    public final static String REVERSED   = "\u001B[7m";

    public final static String BLACK      = "\u001B[30m";
    public final static String RED        = "\u001B[31m";
    public final static String GREEN      = "\u001B[32m";
    public final static String YELLOW     = "\u001B[33m";
    public final static String BLUE       = "\u001B[34m";
    public final static String MAGENTA    = "\u001B[35m";
    public final static String CYAN       = "\u001B[36m";
    public final static String WHITE      = "\u001B[37m";


    public final static String BK_BLACK   = "\u001B[40m";
    public final static String BK_RED     = "\u001B[41m";
    public final static String BK_GREEN   = "\u001B[42m";
    public final static String BK_YELLOW  = "\u001B[43m";
    public final static String BK_BLUE    = "\u001B[44m";
    public final static String BK_MAGENTA = "\u001B[45m";
    public final static String BK_CYAN    = "\u001B[46m";
    public final static String BK_WHITE   = "\u001B[47m";

    public final static String BB_BLACK   = "\u001B[40m;1m";
    public final static String BB_RED     = "\u001B[41m;1m";
    public final static String BB_GREEN   = "\u001B[42m;1m";
    public final static String BB_YELLOW  = "\u001B[43m;1m";
    public final static String BB_BLUE    = "\u001B[44m;1m";
    public final static String BB_MAGENTA = "\u001B[45m;1m";
    public final static String BB_CYAN    = "\u001B[46m;1m";
    public final static String BB_WHITE   = "\u001B[47m;1m";


}

