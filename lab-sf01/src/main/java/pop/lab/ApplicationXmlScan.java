package pop.lab;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pop.lab.service.Greeter;

import java.util.Arrays;

public class ApplicationXmlScan {

  public static void main(String[] args) {
    System.out.println("\n######### .... ...:: xml_scan ::... ... ##########\n");
    ApplicationContext context =
      new ClassPathXmlApplicationContext(new String[] {"application-context-scan.xml"});

   Greeter hello = (Greeter) context.getBean("Greeter");
   Greeter hello2 = (Greeter) context.getBean(Greeter.class);

  System.out.println("\n .............m=|'_'|=m.............\n");
  hello.greet("\n    I am your first bean /-_-\\.");
  hello2.greet("\n    I am your second bean \\^_^/.");
  System.out.println("\n   The two instances are the same ? "
      + ((hello==hello2)
      ? " Yes, are the same, it's a singleton"
      : " Not, aren't the same, they are prototypes!"));
  System.out.println("\n ....................................\n");

  String[] beans= context.getBeanDefinitionNames();
  Arrays.sort(beans);
  for (String bean: beans) {
    System.out.println(" > "+ bean);
  }


    System.out.println("\n ....................................\n");
  }

}
