package pop.lab.service.impl;

import pop.lab.service.Greeter;

import org.springframework.stereotype.Component;

@Component("Greeter")
public class GreeterImpl implements Greeter {

  public void greet(String message) {
    System.out.println("   hello ! " + message );
  }

	public static final Greeter newInstance() {
		return new GreeterImpl();
	}
}
