package pop.lab;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import pop.lab.service.*;

public class ApplicationNotationConfig {

  public static void main(String args[]) {
    System.out.println("\n\n ______________________ m=/'u'\\=m ________________________\n\n");
    ApplicationContext ctx =
      new AnnotationConfigApplicationContext(PopAppConfigurator.class);
    PopAppConfigurator app = ctx.getBean(PopAppConfigurator.class);
    app.run();

    PopAppBanner banner = ctx.getBean(PopAppBanner.class);
    banner.displayBanner();

    app.getBanner().displayBanner();

    System.out.println("\n\n ------------------------------------------------------- \n\n\n");
  }
}

class PopAppBanner {

  public void displayBanner() {
    System.out.println(" ....... m=|-_-|=m ....... ");
    return;
  }
}

@Configuration
@ComponentScan(basePackages = "pop.lab.service")
class PopAppConfigurator {

  @Autowired
  private Greeter greeter;

  @Autowired
  private PopAppBanner banner;

  @Bean
  public PopAppBanner createBanner() {
    return new PopAppBanner();
  }

  public PopAppBanner getBanner() {
    return this.banner;
  }

  public void run() {
    this.banner.displayBanner();
    this.greeter.greet(" I am a greeter spring bean, /'_'/ ");
  }

}
