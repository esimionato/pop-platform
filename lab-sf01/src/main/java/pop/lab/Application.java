package pop.lab;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pop.lab.service.Greeter;

public class Application {

  public static void main(String[] args) {
    System.out.println("\n#########.... /^_^/ ....##########\n");
    ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"application-context.xml"});

    Greeter hello = (Greeter) context.getBean("Greeter");
    Greeter hello2 = (Greeter) context.getBean(Greeter.class);

    System.out.println("\n ...................................\n");
    hello.greet("\n    I am your first bean /-_-\\.");
    hello2.greet("\n    I am your second bean \\^_^/.");
    System.out.println("\n   The two instances are the same ? "
        + ((hello==hello2)
        ? " Yes, are the same, it's a singleton"
        : " Not, aren't the same, they are prototypes!"));
    System.out.println("\n ....................................\n");
  }

}
